import { ImageSize } from "src/__data__/model";

export default interface Image {
  "#text": string;
  size: ImageSize;
}