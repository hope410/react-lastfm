type Size = "small" | "medium" | "large" | "extralarge" | "mega";

interface Image {
  "#text": string;
  size: Size;
}

export default interface Artist {
  name: string;
  listeners: string;
  mbid: string;
  url: string;
  streamable: string;
  image: Image[];
}
