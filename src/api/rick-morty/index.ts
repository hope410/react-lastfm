import axios from 'axios';
import qs from 'querystring';

const BASE = 'https://rickandmortyapi.com/api/';

interface RickMortyResponse<T extends object> {
  results: T[];
}

class RickMorty {
  static async fetch<T extends object>(method: string, query?: Record<string, string>) {
    const { data } = await axios.get<
      RickMortyResponse<T>
    >(
      `${BASE}/${method}?${qs.stringify({
        ...query
      })}`
    );
    
    return data.results;
  }
}

export default RickMorty;