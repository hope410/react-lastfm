import axios from 'axios';
import qs from 'querystring';

const BASE = 'http://ws.audioscrobbler.com/2.0/';
const API_KEY = '97d3b40be1fdaababff32ef1ad9a3d0f';

interface LastFMResponse<T extends object, K1 extends string, K2 extends string> {
  results: {
    [key in K1]: {
      [key in K2]: T[]
    }
  }
}

class LastFM {
  static async fetch<T extends object, K1 extends string, K2 extends string>(method: string, query: Record<string, string>) {
    const { data } = await axios.get<
      LastFMResponse<T, K1, K2>
    >(
      `${BASE}?${qs.stringify({
        method,
        api_key: API_KEY,
        format: 'json',
        ...query
      })}`
    );
    
    return data;
  }
}

export default LastFM;