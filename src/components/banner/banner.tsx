import React from 'react';
import cls from 'classnames';

import { Size } from '../../__data__/model';

import style from './style.css';

type ButtonProps = {
    src: string;
    bgColor?: string;
    size?: Size;
}

const Banner: React.FC<ButtonProps> = ({ src, bgColor, size }) => {
    return (
        <div
            className={cls(
                style[`size${size}`],
                style.wrapper,
            )}
            style={{ backgroundColor: bgColor }}
        >
            <img src={src} className={style.image} />
        </div>
    )
}


Banner.defaultProps = {
    bgColor: '#3c3c3e',
    size: Size.md,
}

export {
    Banner
}
