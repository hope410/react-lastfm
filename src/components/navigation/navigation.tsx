import React from 'react';
import { Link } from 'react-router-dom';
import { NavigationRoute } from 'src/__data__/model';

import style from './style.css';

export interface INavigationProps {
  routes: NavigationRoute[];
  children: any;
}

export default function Navigation (props: INavigationProps) {
  return (
    <>
      <nav className={style.navigation}>
        {props.routes.map(
          (route, idx) => (
            <div key={`route_${idx}`} className={style.navigation__item}>
              <Link to={route.path}>{route.name}</Link>
            </div>
          )
        )}
      </nav>
      {props.children}
    </>
  );
}
