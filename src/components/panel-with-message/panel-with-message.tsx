import React from 'react'
import classnames from 'classnames'

import style from './style.css';

type Props = {
    className?: string
}

export const PanelWithMessage: React.FC<Props> = ({ children, className }) => (
    <div className={classnames(style.wrapper, className)}>
       <span className={style.message}>{children}</span>
    </div>
)
