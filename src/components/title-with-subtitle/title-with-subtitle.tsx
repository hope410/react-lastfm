import React from 'react'
import classnames from 'classnames'

import style from './style.css';

type Props = {
    subtitle:string,
    title:string,
    className?: string
}

export const TitleWithSubtitle: React.FC<Props> = ({ children, className, title, subtitle }) => (
    <div className={classnames(style.wrapper, className)}>
        <span className={style.subtitle}>{subtitle}</span>
        <p className={style.title}>{title}</p>
    </div>
)
