import React from 'react';
import cls from 'classnames';

import { Size } from '../../__data__/model';

import style from './style.css';

export enum LinkColorSheme {
    green = 'green',
    black = 'black',
}

type LinkProps = {
    size?: Size;
    href: string;
    colorSheme?: LinkColorSheme;
    className?: string;
    type?: 'link' | 'button';
};

export type LinkType = React.FC<LinkProps>

export const Link: LinkType = ({
    type,
    size,
    href,
    children,
    colorSheme,
    className
}) => {
    return (
        <a
            className={cls(
                style.commoon,
                style[`type-${type}`],
                style[`color-${colorSheme}`],
                style[`size-${size}`],
                className
            )}
            href={href}
        >
            {children}
        </a>
    )
}

Link.defaultProps = {
    size: Size.md,
    colorSheme: LinkColorSheme.green,
    type: 'link'
}
