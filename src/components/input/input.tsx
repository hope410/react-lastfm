import React from 'react';
import PropTypes from 'prop-types';
import cls from 'classnames';

import { Link } from '../../__data__/model';

import style from './style.css'

interface Props {
    id: string | number;
    name: string;
    label?: string;
    type?: string;
    link?: Link;
    inputRef?: React.RefObject<HTMLInputElement>;
}

type InputProps = Props & Omit<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>, 'id'>;

export const Input: React.FC<InputProps> = ({ inputRef, className, label, id, link, name, type, ...rest }) => (
    <div className={cls(style.wrapper, className)}>
        <div className={style.infoBlock}>
            {label && <label className={style.label} htmlFor={String(id)}>{label}</label>}
            {link && <a className={style.link} href={link.href}>{link.label}</a>}
        </div>

        <input className={style.field} ref={inputRef} type={type} name={name} id={String(id)} {...rest} />
    </div>
)

Input.propTypes = {
    label: PropTypes.string,
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['text', 'password']),
}

Input.defaultProps = {
    type: 'text'
}
