import React from 'react'
import classnames from 'classnames'

import style from './style.css';

type Props = {
    className?: string
}

export const RestoreLoginForm: React.FC<Props> = ({ children, className }) => (
    <div className={classnames(style.wrapper, className)}>
        {children}
    </div>
)
