import React from 'react';

import {
    Input,
    Button,
    ErrorBoundary,
    Link,
    LinkColorSheme,
    Banner,
} from '../../components';
import { Logo, cam } from '../../assets'

import style from './style.css';
import { Size } from 'src/__data__/model';

class Login extends React.PureComponent {
    firstInputRef = React.createRef<HTMLInputElement>();

    state = {
        login: ''
    }

    componentDidMount() {
        this.firstInputRef.current.focus();
    }

    handleLoginInputChange = (event) => {
        const { value } = event.target

        this.setState({
            login: value
        })
    }

    render() {
        const { login } = this.state

        return (
            <ErrorBoundary>
                <div className={style.wrapper}>
                    <form className={style.loginForm}>
                        <h4 className={style.mark}>
                            <img className={style.logo} src={Logo} />
                            MENTOR
                        </h4>
                        <Input
                            className={style.loginField}
                            inputRef={this.firstInputRef}
                            label="Логин"
                            link={{
                                href: '/recover/login',
                                label: 'Забыли логин?'
                            }}
                            id="login-input"
                            name="login"
                            value={login}
                            onChange={this.handleLoginInputChange}
                        />
                        <Input
                            label="Пароль"
                            id="password-input"
                            link={{
                                href: '/recover/loogin',
                                label: 'Забыли пароль?'
                            }}
                            name="password"
                            type="password"
                        />
                        <Button
                            type="submit"
                            className={style.offsetMd}
                        >
                            Продолжить
                        </Button>
                        <Link
                            type="button"
                            href="/register"
                            colorSheme={LinkColorSheme.black}
                        >
                            Зарегистрироваться
                        </Link>
                    </form>
                    <Banner size={Size.lg} src={cam}  />
                </div>
            </ErrorBoundary>
        )
    }
}

export default Login
