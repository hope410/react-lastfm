import * as React from 'react';
import Character from 'src/api/models/Character';
import RickMorty from 'src/api/rick-morty';

import style from './style.css';

export interface ICharactersProps {
}

export default function Characters (props: ICharactersProps) {
  const [data, setData] = React.useState<Character[]>([]);

  React.useEffect(
    () => {
      RickMorty
        .fetch<Character>('character')
        .then(setData) // res => setData(res)
        .catch(
          e => {
            alert(e.message);
            console.error(e);
          }
        );
    },
    []
  );

  return (
    <div>
      Characters

      <div className={style.container}>
        {data.map(
          (item, key) => (
            <div className={style.item} key={`item_${key}`}>
              <img src={item.image} alt=""/>
            </div>
          )
        )}
      </div>
    </div>
  )
}
