import * as React from 'react';
import LastFM from 'src/api/last-fm';
import Album from 'src/api/models/Album';

export interface IAlbumsProps {
}

export default function Albums (props: IAlbumsProps) {
  const [data, setData] = React.useState<Album[]>([]);

  React.useEffect(
    () => {
      LastFM
        .fetch<Album, 'albummatches', 'album'>('album.search', {
          album: 'believe'
        })
        .then(
          res => setData(res.results.albummatches.album)
        )
        .catch(
          e => {
            alert(e.message);
            console.error(e);
          }
        );
    },
    []
  )

  return (
    <div>
      Albums

      {data.map(
        (item, key) => (
          <div className="album" key={`item_${key}`}>
            <div className="title">
              {item.artist} - {item.name}
            </div>
            <div className="image">
              <img src={item.image[0]["#text"]} alt=""/>
            </div>
          </div>
        )
      )}
    </div>
  );
}
