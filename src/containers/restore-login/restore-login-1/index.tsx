import React, {useEffect} from 'react';

import {
    Input,
    Button,
    ErrorBoundary,
    ButtonColorSheme,
    PanelWithMessage,
    TitleWithSubtitle
} from '../../../components';
import {Logo} from '../../../assets'

import style from './style.css';
import classnames from 'classnames'

interface Props {

}

const RestoreLogin: React.FC<Props> = () => {
    const firstInputRef = React.createRef<HTMLInputElement>();

    useEffect(() => {
        firstInputRef.current.focus();
    }, [])


    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <div className={style.sidebar}>
                    <div className={style.sidebarContent}>
                        <img src={Logo} alt="logo" className={classnames(style.logo, style.offset)}/>
                        <form className={style.emailForm}>
                            <TitleWithSubtitle
                                title={"Введите email"}
                                subtitle={"Восстановление login"}
                                className={style.offsetMd}
                            />
                            <Input
                                inputRef={firstInputRef}
                                label="Электронная почта"
                                id="email-input"
                                name="email"
                            />
                            <Button
                                type="submit"
                                className={style.offsetMd}
                                colorScheme={ButtonColorSheme.green}
                            >
                                Продолжить
                            </Button>
                            <a className={style.cancel} href="/login">Отмена</a>
                        </form>
                    </div>
                </div>
                <PanelWithMessage className={style.rightPanel}>
                    На введённый email прийдёт код подтверждения
                </PanelWithMessage>
            </div>
        </ErrorBoundary>
    )

}

export default RestoreLogin
