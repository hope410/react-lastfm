import React, {useEffect} from 'react';

import {
    LabeledInput,
    Button,
    ErrorBoundary,
    ButtonColors,
    TextWithLines
} from '@main/components';
import {Logo} from '@main/assets'

import style from './style.css';

interface Props {

}

const RestoreLogin: React.FC<Props> = () => {
    const firstInputRef = React.createRef<HTMLInputElement>();

    useEffect(() => {
        firstInputRef.current.focus();
    }, [])


    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <form className={style.loginForm}>
                    <h4 className={style.mark}>
                        <img className={style.logo} src={Logo}/>
                        STC mentor
                    </h4>
                    <h3 className={style.title}>Вход в STC mentor</h3>
                    <Button className={style.offsetMd} colorScheme={ButtonColors.blue}>Войти с помощью Google</Button>
                    <TextWithLines className={style.or}>или</TextWithLines>
                    <LabeledInput
                        inputRef={firstInputRef}
                        label="Введите логин"
                        id="login-input"
                        name="login"
                    />
                    <LabeledInput
                        label="Введите пароль"
                        id="password-input"
                        name="password"
                        type="password"
                    />
                    <div className={style.forgotWrapper}>
                        <a className={style.forgot} href="/recover">Забыли пароль?</a>
                    </div>
                    <Button
                        type="submit"
                        className={style.offsetMd}
                        colorScheme={ButtonColors.purple}
                    >
                        Продолжить
                    </Button>
                    <a className={style.reg} href="/register">Зарегистрироваться</a>
                </form>
            </div>
        </ErrorBoundary>
    )

}

export default RestoreLogin
