import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './app.css'

import routes from './__data__/navigation-routes';
import Navigation from './components/navigation';

const App = () => (
    <Router>
        <Navigation routes={routes}>
            <Switch>
                {routes.map(
                    route => (
                        <Route path={route.path}>
                            <route.Component />
                        </Route>
                    )
                )}
            </Switch>
        </Navigation>
    </Router>
);

export default App;
