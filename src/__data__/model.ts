export type Link = {
    href: string;
    label: string;
}

export enum Size {
    lg = 'lg',
    md = 'md',
    sm = 'sm',
    xs = 'xs',
}

export type ImageSize = "small" | "medium" | "large" | "extralarge" | "mega";

export type NavigationRoute = {
    name: string;
    path: string;
    Component: React.ComponentType;
}