import { NavigationRoute } from "./model";

import Albums from "src/containers/albums";
import Artists from "src/containers/artists";
import Tracks from "src/containers/tracks";
import Characters from "src/containers/characters";

const routes: NavigationRoute[] = [
  {
    name: 'Artists',
    path: '/artists',
    Component: Artists
  },
  {
    name: 'Albums',
    path: '/albums',
    Component: Albums
  },
  {
    name: 'Tracks',
    path: '/tracks',
    Component: Tracks
  },
  {
    name: 'Charcters',
    path: '/characters',
    Component: Characters
  }
]

export default routes;